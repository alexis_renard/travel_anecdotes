import {StyleSheet} from 'react-native';

export const behindTextBackgroundColor = '#EFEEEE'
// export const behindTextBackgroundColor = '#222222'
export const mainTextColor = '#222222'
// export const mainTextColor = '#EFEEEE'

export const defaultIconColor = '#000000'
export const defaultIconBackgroundColor='#FFFFFF'
export const defaultButtonBackgroundColor = '#E3E3E2'

export const lightGreyButtonBackgroundColor = '#EFEEEE'
export const redButtonBackgroundColor = '#9A0800'
export const obscureRedButtonBackgroundColor = '#580400'
export const redBorderBackgroundColor = '#9A0800'
export const headerBackgroundColor = '#9A0800'
export const secondaryTextColor='#EFEEEE'
export const logoPrimaryColor = '#DA3500'
export const logoSecondaryColor = '#696969'

export const anecdotesListItemBorderColor = '#9A0800'
export const anecdotesListItemBackgroundColor = '#E3E2E2'
export const anecdotesListItemTextColor = '#3E0C8F'

export const modalButtonBackgroundColor = '#EFEEEE'

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    //flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: behindTextBackgroundColor,
    padding: 15,
  },
  container: {
    flex: 1,
    //flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: behindTextBackgroundColor,
  },
  imageHeaderLogo: {
    flex:1,
    aspectRatio:1,
    // marginTop: 2, //using the margin to resize the logo
    marginRight: 5,
    marginBottom: 2,
  },
  randomAnecdote: {
    fontSize: 40,
    color: mainTextColor,
  },
  randomAnecdoteContainer: {
    flex: 6,
    justifyContent: 'center',
    flexDirection: 'column',
    textAlign: 'center',
    backgroundColor: behindTextBackgroundColor
  },
  emptyFlatListMessageText:{
    marginTop: 15,
    padding: 20,
    fontSize: 22,
    // color:'red'
  },
  emptyFlatListMessageSmallerText:{
    padding: 20,
    marginBottom: 15,
    fontSize: 17,
    // color:'red'
  },
  emptyFlatListButtonText: {
    paddingTop:7,
    paddingRight:7,
    paddingBottom:7,
    paddingLeft:7,
    // paddingRight: 15,
    color: mainTextColor,
    // fontWeight: '500',
    fontSize: 16
  },
  homeScreenAppDescriptionTitle: {
    fontSize: 35,
    textAlign: 'center',
    marginBottom: 60,
    color: mainTextColor
  },
  homeScreenAppDescriptionText: {
    fontSize: 20,
    textAlign: 'center',
    // fontStyle: 'italic',
    color: mainTextColor,
  },
  homeScreenAppDescriptionTitleContainer: {
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center',
    color: mainTextColor,
  },
  homeScreenAppDescriptionTextSmaller: {
    fontSize: 17,
    textAlign: 'center',
    color: mainTextColor,
  },
  homeScreenAppDescriptionTextContainer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    color: mainTextColor,
  },
  homeScreenAppDescriptionContainer: {
    flex: 4,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    color: mainTextColor,
  },
  homeScreenButton: {
    width: '100%',
    paddingTop:10,
    paddingBottom:10,
    backgroundColor: redButtonBackgroundColor,
    borderRadius:7,
  },
  homeScreenButtonText: {
    paddingLeft: 70,
    paddingRight: 70,
    color: secondaryTextColor,
    //fontWeight: '500',
    fontSize: 18
  },
  homeScreenButtonContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    textAlign: 'center',
  },
  randomAnecdoteInstructions: {
    flex: 6,
    fontSize: 20,
    textAlign: 'center',
    fontStyle: 'italic',
    //paddingTop:30, //in order to have the side arrow well positionned
    color: logoSecondaryColor,
  },
  instructionsContainer: {
    flex: 1.5,
    justifyContent: 'center',
    flexDirection: 'row',
    textAlign: 'center',
    //backgroundColor: '#F5FCFF',
    //borderRadius: 40,
  },
  anecdotesListContainer: {
    flex: 1,
    flexDirection: 'row',
    // borderWidth: 1,
    // borderColor: anecdotesListItemBorderColor,
    backgroundColor: anecdotesListItemBackgroundColor,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 10
  },
  categoryTitle: {
    margin: 10,
    fontSize: 30,
    color: mainTextColor
  },
  anecdotesListItem: {
    marginTop: 15,
    marginBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    paddingLeft: 15,
    paddingRight: 15,
    color: mainTextColor,
    fontSize: 15
  },


  /* BEGIN MODAL SECTION */
  // wrapper: {
  //   paddingTop: 50,
  //   flex: 1
  // },
  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalCenter: {
    height: 400,
    width: 300,
    borderRadius:30
  },
  modalSuccess: {
    height: 100,
    width: 100,
    borderRadius:100,
    backgroundColor: redBorderBackgroundColor,
  },
  modalBottom: {
    height: 250
  },
  modalHomeScreenContainer: {
    flex:1,
    flexDirection:'column',
    justifyContent: 'center',
  },
  modalHomeScreenButtonContainer: {
    // width: '80%',
    marginTop: 10,
    marginBottom: 10,
  },
  modalHomeScreenButtonText: {
    paddingTop:10,
    paddingRight:10,
    paddingBottom:10,
    paddingLeft:5,
    // paddingRight: 15,
    color: mainTextColor,
    // fontWeight: '500',
    fontSize: 17
  },
  modalAnecdoteDetailMainContainer: {
    width: "100%",
    flex:1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    padding:10,
  },
  modalAnecdoteDetailAnecdoteTitle: {
    padding:10
  },
  modalAnecdoteDetailAnecdoteButtonText: {
    paddingTop:7,
    paddingRight:10,
    paddingBottom:7,
    paddingLeft:3,
    color: mainTextColor,
    // fontWeight: '500',
    fontSize: 16
  },
  modalAnecdoteDetailAnecdoteCountry: {
    flex:1,
    alignSelf:'flex-end',
    padding:10,
    fontStyle: 'italic'
  },
  modalAnecdoteDetailButtonsMainContainer: {
    flex:1,
    flexDirection:'row',
    paddingRight: 10,
    paddingLeft: 10,
    marginBottom:15
  },
  modalAnecdoteDetailButtonContainer: {
    flex: 1,
    marginBottom: 5,
    marginRight: 5,
    marginLeft: 5,
  },
  modalSuccessText: {
    color: secondaryTextColor
  },
  /* END MODAL SECTION */
  debugBorder: {
    borderWidth: 3,
    borderColor: 'red'
  },
  debugBorderBlue: {
    borderWidth: 3,
    borderColor: 'blue'
  }
});
