class Landscape extends Component {
  render() {
    return (
        <View style={styles.container}>
          <Image
            source={require('./static/images/landscape.jpg')}
            style={{flex:1, aspectRatio: 1}}
            resizeMode="contain"
          />
          <Text style={styles.instructions}>Beautiful landscape </Text>
        </View>
    );
  }
}

class ScreenCustomNavigation extends React.Component {
  static navigationOptions = {
    title: 'Travel Anecdotes',
    headerStyle: {
      backgroundColor: '#f45112',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
  render() {
    return (
      <Text style={styles.welcome}>Vous arrivez d'ici depuis {this.props.navigation.getParam('source','un endroit inconnu, bien ouej')}</Text>
    );
  }
}

class List extends Component  {
  render() {
    return (
      <View style={styles.container}>
      <FlatList
      data={[
        {key: 'Devin'},
        {key: 'Jackson'},
        {key: 'James'},
        {key: 'Joel'},
        {key: 'John'},
        {key: 'Jillian'},
        {key: 'Jimmy'},
        {key: 'Julie'},
      ]}
      renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
      />
      </View>
    );
  }
}

const instructions = Platform.select({
  ios: 'Vous trouverez ici mes anecdotes de voyage.\n\n' +
  'Parcourez-les, et choisissez celle que vous voulez que je vous raconte !',
  android: 'Vous trouverez ici mes anecdotes de voyage.\n\n' +
  'Parcourez-les, et choisissez celle que vous voulez que je vous raconte !'
});

headerRight: (<View>
                <Button
                  icon={
                    <Icon
                      name="arrow-right"
                      size={15}
                      color={Constants.buttonAndHeaderBackgroundColor}
                    />
                  }
                  iconRight
                  title="Button with right icon"
                />
              </View>)


onLongPress={()=> {
  Alert.alert(
    this.props.anecdoteReducer.homescreenAnecdote.title,
    'Localisation : '+this.props.anecdoteReducer.homescreenAnecdote.country,
    [
      {
        text: 'Déjà Racontée !',
        onPress: () => { this.props.addAnecdoteAlreadyTold(this.props.anecdoteReducer.homescreenAnecdote) },
        },
      {
        text: 'A raconter',
        onPress: () => { this.props.addAnecdoteToBeTold(this.props.anecdoteReducer.homescreenAnecdote) },
        },
      ],
  );
}

<View style={Constants.styles.homeScreenAppDescriptionTextContainer}>
  <Text style={Constants.styles.homeScreenAppDescriptionTextSmaller}>Parcourez-les, marquez les "A Raconter", bref amusez vous à préparer une petite liste d'anecdotes que je vous raconterai à notre prochaine rencontre.</Text>
</View>
