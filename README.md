# Travel Anecdotes
Because it's always a mess to tell your friends all about your travels. Let them pick the parts they want, and tell them the story behind.  

## How was it built ?
Made with the [React Native](https://reactnative.dev/) technology.

## Where to find it ?
The app (while still in development) is available on the [Google Play Store](https://play.google.com/store/apps/details?id=com.travelanecdotes).