/**
 * @format
 * @flow
 */


import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, StyleSheet, Icon} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Constants from './../constants.js';

export default class ImageHeader extends React.Component<{}> {
  render() {
    return (
      <TouchableOpacity onPress={() => this.refs.modal1.open()}>
        <Image
          source={require('./../static/images/logo_with_suitcase.png')}
          style={{flex:1, aspectRatio:0.9, marginRight:10}}
        />
      </TouchableOpacity>
    );
  }
}
