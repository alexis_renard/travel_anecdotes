/**
 * @format
 * @flow
 */

import React, {Component, View, Text, Image, Button} from 'react';
import { createStackNavigator , createAppContainer , createMaterialTopTabNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
// import Icon from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Constants from './../constants.js';


// Import Own Components //
import HomeScreen from './HomeScreen';
import ImageHeader from './ImageHeader';
import RandomAnecdoteTab from './Tabs/RandomAnecdoteTab';
import ToBeToldAnecdotesListTab from './Tabs/ToBeToldAnecdotesListTab';
import AllAnecdotesListTab from './Tabs/AllAnecdotesListTab';
// import { MaterialHeaderButtons, Item } from './SettingsOptions/MyHeaderButtons'
import AlreadyToldAnecdotesList from './SettingsOptions/AlreadyToldAnecdotesList';
// ----------------- //

const AnecdoteTabs = createMaterialBottomTabNavigator(
    {
      RandomAnecdote: {
        screen: RandomAnecdoteTab,
      },
      ToBeToldAnecdotes: {
        screen: ToBeToldAnecdotesListTab,
      },
      AlreadyToldAnecdotes: {
        screen: AlreadyToldAnecdotesList,
      },
      AllAnecdotes: {
        screen: AllAnecdotesListTab,
      },
    },
    {
      initialRouteName: 'AllAnecdotes',
      activeColor: '#FFFFFF',
      inactiveColor: '#BDBDBD',
      // tabBarPosition: 'bottom',
      shifting: true,
      barStyle: {
          backgroundColor: Constants.headerBackgroundColor,
          height: 52,
      }
    }
)

const StackNavigator = createStackNavigator(
    {
      Home: {
        screen: HomeScreen,
      },
      Anecdotes: {
        screen: AnecdoteTabs,
        navigationOptions: ({navigation}) => ({
          headerTintColor: Constants.secondaryTextColor, //for the color of the arrow
          headerStyle: { backgroundColor: Constants.headerBackgroundColor },
          //headerTitle: <ImageHeader/>,
          // headerRight: <Icon.Button
          //                 name="more-vert"
          //                 color={Constants.secondaryTextColor}
          //                 backgroundColor={Constants.headerBackgroundColor}
          //                 onPress={()=>navigation.navigate('AlreadyToldAnecdotes', {source: 'Anecdotes Tabs'})}>
          //              </Icon.Button>
          // headerRight: <ImageHeader />
        }),
      }
    },
    {
      initialRouteName: "Home",
      defaultNavigationOptions: {
      },
    }
);

export default createAppContainer(StackNavigator);
