/**
 * @format
 * @flow
 */


import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, StyleSheet, Icon} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Constants from './../constants.js';

export default class ImageHeader extends React.Component<{}> {
  render() {
    return (
      <TouchableOpacity>
        <Image
          source={require('./../static/images/logo_with_suitcase_red.png')}
          style={Constants.styles.imageHeaderLogo}
        />
      </TouchableOpacity>
    );
  }
}
