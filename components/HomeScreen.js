/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, FlatList, Text, View, Image, ImageBackground, Alert, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modalbox'
import Emoji from 'react-native-emoji';

import * as Constants from './../constants.js';

import { connect } from 'react-redux';

export class HomeScreen extends React.Component<{}> {

  static navigationOptions = {
    title: 'Travel Anecdotes',
    headerTitleStyle: {
      paddingLeft:17,
	    // alignSelf:'left',
      textAlign: 'left',
      // width: '90%',
      color: '#FFFFFF',
      fontWeight:'400',
      fontSize: 20
    },
    headerStyle: {
      backgroundColor: Constants.headerBackgroundColor,
      height: 70,
    },
    headerLeft: (
      <View>
        <Image
          source={require('./../static/images/logo_with_suitcase_red.png')}
          style={{flex:1, aspectRatio: 0.8, marginLeft:10}}
        />
      </View>
    ),
  };

  constructor() {
    super();
    this.state = {
      isOpen: false,
      swipeToClose: true,
    };
  }

  generateButton(type) {
    switch (type) {
      case "GenerateAnecdote":
        return <Icon.Button
            name="autorenew"
            color={Constants.defaultIconColor}
            backgroundColor={Constants.modalButtonBackgroundColor}
            onPress={()=> { this.props.navigation.navigate('RandomAnecdote', {source: 'La Maison'})}}
        >
          <Text style={Constants.styles.modalHomeScreenButtonText}>Générer une anecdote</Text>
        </Icon.Button>
        break;
      case "ToBeToldAnecdotes":
        if(this.props.anecdoteReducer.toBeTold.length==0){  //si aucune anecdote ne se trouve dans cette liste
          return <Icon.Button
              style={{opacity:0.2}}
              name="chat-bubble-outline"
              color={Constants.defaultIconColor}
              backgroundColor={Constants.modalButtonBackgroundColor}
          >
            <Text style={Constants.styles.modalHomeScreenButtonText}>Anecdotes à raconter</Text>
          </Icon.Button>
        }
        return <Icon.Button
            name="chat-bubble-outline"
            color={Constants.defaultIconColor}
            backgroundColor={Constants.modalButtonBackgroundColor}
            onPress={()=> { this.props.navigation.navigate('ToBeToldAnecdotes', {source: 'La Maison'})}}
        >
          <Text style={Constants.styles.modalHomeScreenButtonText}>Anecdotes à raconter</Text>
        </Icon.Button>
        break;
        case "AlreadyToldAnecdotes":
          const opacity=1;
          if(this.props.anecdoteReducer.alreadyTold.length==0){//si aucune anecdote ne se trouve dans cette liste
            return <Icon.Button
                style={{opacity:0.2}}
                name="check"
                color={Constants.defaultIconColor}
                backgroundColor={Constants.modalButtonBackgroundColor}
            >
              <Text style={Constants.styles.modalHomeScreenButtonText}>Anecdotes déjà racontées</Text>
            </Icon.Button>
          }
          return <Icon.Button
              name="check"
              color={Constants.defaultIconColor}
              backgroundColor={Constants.modalButtonBackgroundColor}
              onPress={()=> { this.props.navigation.navigate('AlreadyToldAnecdotes', {source: 'La Maison'})}}
          >
            <Text style={Constants.styles.modalHomeScreenButtonText}>Anecdotes déjà racontées</Text>
          </Icon.Button>
          break;
        case "AllAnecdotes":
          return <Icon.Button
              name="public"
              // name="reorder"
              color={Constants.defaultIconColor}
              backgroundColor={Constants.modalButtonBackgroundColor}
              onPress={()=> { this.props.navigation.navigate('AllAnecdotes', {source: 'La Maison'})}}
          >
            <Text style={Constants.styles.modalHomeScreenButtonText}>Voir toutes les anecdotes</Text>
          </Icon.Button>
          break;
      default:
        return <Icon.Button><Text>Supposed to be a wonderful working button</Text></Icon.Button>
    }
  }

  render() {

    return (
      <ImageBackground source={require('../static/images/landscape.jpg')} style={{width: '100%', height: '100%'}}>
        <View style={Constants.styles.mainContainer}>
          <View style={Constants.styles.homeScreenAppDescriptionContainer}>
              <View style={Constants.styles.homeScreenAppDescriptionTitleContainer}>
                <Text style={Constants.styles.homeScreenAppDescriptionTitle}>Bienvenue sur Travel Anecdotes !</Text>
                <Text style={Constants.styles.homeScreenAppDescriptionText}>Vous trouverez ici l'intégralité des anecdotes qui ont composé mon voyage d'un an en Amérique du Sud.{"\n\n"}</Text>
              </View>
          </View>
          <View style={Constants.styles.homeScreenButtonContainer}>
            <View style={Constants.styles.homeScreenButton}>
              <TouchableOpacity
                onPress={() => this.refs.modal.open()}
                activeOpacity={0.6}
              >
                <Text style={Constants.styles.homeScreenButtonText}>Commencer  <Emoji name="rocket" style={{fontSize: 18}}/></Text>
              </TouchableOpacity>
            </View>
          </View>
          <Modal
            style={[Constants.styles.modal, Constants.styles.modalCenter]}
            position={"center"}
            ref={"modal"}
            swipeToClose={this.state.swipeToClose}
          >
            <View style={Constants.styles.modalHomeScreenContainer}>
              <View style={{marginBottom:10}}>
                <Text>Par où commencer ?</Text>
              </View>
              <View style={Constants.styles.modalHomeScreenButtonContainer}>
                {this.generateButton("GenerateAnecdote")}
              </View>

              <View style={Constants.styles.modalHomeScreenButtonContainer}>
                {this.generateButton("ToBeToldAnecdotes")}
              </View>

              <View style={Constants.styles.modalHomeScreenButtonContainer}>
                {this.generateButton("AlreadyToldAnecdotes")}
              </View>

              <View style={Constants.styles.modalHomeScreenButtonContainer}>
                {this.generateButton("AllAnecdotes")}
              </View>

            </View>
          </Modal>
        </View>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
  const { anecdoteReducer } = state
  return { anecdoteReducer }
};

export default connect(mapStateToProps)(HomeScreen);
