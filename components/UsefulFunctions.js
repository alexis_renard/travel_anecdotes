/**
 * @format
 * @flow
 */


import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import Modal from 'react-native-modalbox';
import * as Constants from './../constants.js';
import Icon from 'react-native-vector-icons/MaterialIcons';

export function createViewContainingTwoButtons(
  textLeftButton,
  textRightButton,
  onPressLeftButton,
  onPressRightButton,
  disabledLeftButton=false,
  disabledRightButton=false){

  let view=<View></View>;

  let styleLeftButton = styleRightButton = {paddingLeft:10, paddingRight:10};

  if (disabledLeftButton) {
    let styleLeftButton = {paddingLeft:10, paddingRight:10, opacity:0.2};
  }
  if (disabledRightButton) {
    let styleRightButton = {paddingLeft:10, paddingRight:10, opacity:0.2};
  }

  // RETURN THE TEMPLATED BUTTON

  return (<View style={Constants.styles.modalAnecdoteDetailButtonsMainContainer}>
              <View style={Constants.styles.modalAnecdoteDetailButtonContainer}>
                <Icon.Button
                disabled={disabledLeftButton}
                style={styleLeftButton}
                name="chat-bubble-outline"
                color={Constants.defaultIconColor}
                backgroundColor={Constants.lightGreyButtonBackgroundColor}
                onPress= {onPressLeftButton}
                >
                  <Text style={Constants.styles.modalAnecdoteDetailAnecdoteButtonText}>{textLeftButton}</Text>
                </Icon.Button>
              </View>
              <View style={Constants.styles.modalAnecdoteDetailButtonContainer}>
                <Icon.Button
                disabled={disabledRightButton}
                style={styleRightButton}
                name="check"
                color={Constants.defaultIconColor}
                backgroundColor={Constants.lightGreyButtonBackgroundColor}
                onPress={onPressRightButton}
                >
                  <Text style={Constants.styles.modalAnecdoteDetailAnecdoteButtonText}>{textRightButton}</Text>
                </Icon.Button>
              </View>
            </View>)
}
