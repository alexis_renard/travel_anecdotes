/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, FlatList, Text, View, Image, Button, Alert, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Animatable from 'react-native-animatable';
import Modal from 'react-native-modalbox';
import Emoji from 'react-native-emoji';

import * as Constants from './../../constants.js';
import * as UsefulFunctions from './../UsefulFunctions.js'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { shuffleRandomAnecdote, addAnecdoteToBeTold, addAnecdoteAlreadyTold, removeAnecdoteToBeTold} from './../../actions/AnecdoteActions';

export class ToBeToldAnecdotesListTab extends React.Component<{}> {
  static navigationOptions = {
    tabBarLabel: 'À Raconter',
    tabBarIcon: ({ focused }) => (
      <Icon name="chat-bubble-outline" size={20} color={Constants.secondaryTextColor}/>
    )
  };

  constructor() {
    super();
    this.state = {
      swipeToClose: true,
      clickedAnecdote: {title:'Should be updated soon'}
    };
  }

  openModal(item){
    this.setState({
      clickedAnecdote:item
    });
    this.refs.modal.open();
  }

  closeModal(){
    this.refs.modal.close();
    this.refs.modal_success.open();
    setTimeout(this.refs.modal_success.close,1500);
  }

  generateAddAnecdoteToListsButtons(){
    textLeftButton="Marquer déjà racontée";
    textRightButton="Retirer de la liste";

    let onPressLeftButton = () => { this.props.addAnecdoteAlreadyTold(this.state.clickedAnecdote) ; this.closeModal() ; };
    let onPressRightButton = () => { this.props.removeAnecdoteToBeTold(this.state.clickedAnecdote) ; this.closeModal() ; };

    return UsefulFunctions.createViewContainingTwoButtons(
      textLeftButton,
      textRightButton,
      onPressLeftButton,
      onPressRightButton,
    )
  }

  render() {

    const emptyFlatListMessage =
      <View style={{flex:1, marginTop: 10, marginBottom: 10}}>

        <Text style={Constants.styles.emptyFlatListMessageText}>
          Liste vide ! <Emoji name="scream"/>
        </Text>

        <Text style={Constants.styles.emptyFlatListMessageSmallerText}>
          Aucune anecdote n'a encore été marquée à raconter ? {'\n'}{'\n'}
          N'oubliez pas que vous pouvez générer des anecdotes aléatoirement, ou encore en checker la liste complète.
        </Text>

        <View style={{paddingRight: 10, paddingLeft: 10}}>
          <View style={{marginBottom:5}}>
            <Icon.Button
            style={{paddingLeft:10, paddingRight:10}}
            name="autorenew"
            color={Constants.defaultIconColor}
            backgroundColor={Constants.defaultButtonBackgroundColor}
            onPress={()=> { this.props.navigation.navigate('RandomAnecdote', {source: 'La Maison'})}}
            >
              <Text style={Constants.styles.emptyFlatListButtonText}>Générer une anecdote</Text>
            </Icon.Button>
          </View>

          <View>
            <Icon.Button
            style={{paddingLeft:10, paddingRight:10}}
            name="public"
            // name="reorder"
            color={Constants.defaultIconColor}
            backgroundColor={Constants.defaultButtonBackgroundColor}
            onPress={()=> { this.props.navigation.navigate('AllAnecdotes', {source: 'La Maison'})}}
            >
              <Text style={Constants.styles.emptyFlatListButtonText}>Voir toutes les anecdotes</Text>
            </Icon.Button>
          </View>
        </View>

      </View>;

      const flatList = <FlatList
        data={this.props.anecdoteReducer.toBeTold}
        renderItem={({item}) =>
          <View style={Constants.styles.anecdotesListContainer}>
            <View>
              <TouchableOpacity
                onPress={this.openModal.bind(this, item)}
                onLongPress={this.openModal.bind(this, item)}
                activeOpacity={0.7}
                >
                  <Text style={Constants.styles.anecdotesListItem}>{item.title}</Text>
              </TouchableOpacity>
            </View>
          </View>
        }
        keyExtractor={(item, index) => index.toString()}
      />;

    return (
      <View style={Constants.styles.mainContainer}>
        <View>
          <Text style={Constants.styles.categoryTitle}>À raconter</Text>
        </View>

        {this.props.anecdoteReducer.toBeTold.length==0 ? emptyFlatListMessage : flatList}


        <Modal
          style={[Constants.styles.modal, Constants.styles.modalBottom]}
          position={"bottom"}
          ref={"modal"}
          swipeToClose={this.state.swipeToClose}
        >
          <View style={Constants.styles.modalAnecdoteDetailMainContainer}>
            <View style={Constants.styles.modalAnecdoteDetailAnecdoteTitle}>
              <Text>« {this.state.clickedAnecdote.title} »</Text>
            </View>
            <View style={Constants.styles.modalAnecdoteDetailAnecdoteCountry}>
              <Text><Emoji name="round_pushpin"/> {this.state.clickedAnecdote.country}</Text>
            </View>
            {this.generateAddAnecdoteToListsButtons()}
          </View>
        </Modal>
        <Modal
          style={[Constants.styles.modal, Constants.styles.modalSuccess]}
          position={"center"}
          ref={"modal_success"}
          swipeToClose={this.state.swipeToClose}
        >
          <View>
            <Animatable.View animation="slideInDown"><Icon name="check" color={Constants.behindTextBackgroundColor} size={50}/></Animatable.View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { anecdoteReducer } = state
  return { anecdoteReducer }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    addAnecdoteAlreadyTold,
    addAnecdoteToBeTold,
    removeAnecdoteToBeTold,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ToBeToldAnecdotesListTab);
