/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, FlatList, Text, View, Image, Button, Alert, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Animatable from 'react-native-animatable';
import Modal from 'react-native-modalbox'
import Emoji from 'react-native-emoji';

import * as Constants from './../../constants.js';
import * as UsefulFunctions from './../UsefulFunctions.js'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { shuffleRandomAnecdote, addAnecdoteToBeTold, addAnecdoteAlreadyTold, removeAnecdoteToBeTold, removeAnecdoteAlreadyTold} from './../../actions/AnecdoteActions';

export class AllAnecdotesListTab extends React.Component<{}> {
  static navigationOptions = {
    tabBarLabel: "Toutes",
    tabBarIcon: ({ focused, tintcolor }) => (
      <Icon name="public" size={20}  color={Constants.secondaryTextColor} />
    )
  }

  constructor() {
    super();
    this.state = {
      swipeToClose: true,
      clickedAnecdote: {title:'Should be updated soon'}
    };
  }

  openModal(item){
    this.setState({
      clickedAnecdote:item
    });
    this.refs.modal.open();
  }

  closeModal(){
    this.refs.modal.close();
    this.refs.modal_success.open();
    setTimeout(this.refs.modal_success.close,1500);
  }

  generateAddAnecdoteToListsButtons(){
    const textToBeToldButton="À Raconter";
    const textAlreadyToldButton="Déjà Racontée";

    // CHECK IF ALREADY IN ALREADYTOLD
    const isItAlreadyInToBeTold = this.props.anecdoteReducer.toBeTold.some((anecdote)=>{ // we remove the anecdote from the toBeTold array when marking it as alreadyTold
      return anecdote.title === this.state.clickedAnecdote.title;
    });
    //the style is standard and redirecting to the right action
    let disabledToBeToldButton = false;
    let onPressToBeToldButton = () => { this.props.addAnecdoteToBeTold(this.state.clickedAnecdote) ; this.closeModal() ; };
    if(isItAlreadyInToBeTold){
      // if it is already in the list, need to deactivate the button and the action
      onPressToBeToldButton=() => {};
      disabledToBeToldButton = true;
    }


    // CHECK IF ALREADY IN TOBETOLD
    const isItAlreadyInAlreadyTold = this.props.anecdoteReducer.alreadyTold.some((anecdote)=>{ // we remove the anecdote from the toBeTold array when marking it as alreadyTold
      return anecdote.title === this.state.clickedAnecdote.title;
    });
    //the style is standard and redirecting to the right action
    let disabledAlreadyToldButton = false;
    let onPressAlreadyToldButton = () => { this.props.addAnecdoteAlreadyTold(this.state.clickedAnecdote) ; this.closeModal() ; };
    if(isItAlreadyInAlreadyTold){
      // if it is already in the list, need to deactivate the button and the action
      onPressAlreadyToldButton = () => {};
      disabledAlreadyToldButton = true;
    }
    // RETURN THE TEMPLATED BUTTON
    return UsefulFunctions.createViewContainingTwoButtons(
      textToBeToldButton,
      textAlreadyToldButton,
      onPressToBeToldButton,
      onPressAlreadyToldButton,
      disabledToBeToldButton,
      disabledAlreadyToldButton,
    )
  }


  render() {
    return (
      <View style={Constants.styles.mainContainer}>
        <View>
          <Text style={Constants.styles.categoryTitle}>Toutes les anecdotes</Text>
        </View>
        <FlatList
          data={this.props.anecdoteReducer.allAnecdotes}
          renderItem={({item}) =>
            <View style={Constants.styles.anecdotesListContainer}>
              <TouchableOpacity
                onPress={this.openModal.bind(this, item)}
                onLongPress={this.openModal.bind(this, item)}
                activeOpacity={0.7}
                >
                  <Text style={Constants.styles.anecdotesListItem}>{item.title}</Text>
              </TouchableOpacity>
            </View>
          }
          keyExtractor={(item, index) => index.toString()}
        />
        <Modal
          style={[Constants.styles.modal, Constants.styles.modalBottom]}
          position={"bottom"}
          ref={"modal"}
          swipeToClose={this.state.swipeToClose}
        >
          <View style={Constants.styles.modalAnecdoteDetailMainContainer}>
            <View style={Constants.styles.modalAnecdoteDetailAnecdoteTitle}>
              <Text>« {this.state.clickedAnecdote.title} »</Text>
            </View>
            <View style={Constants.styles.modalAnecdoteDetailAnecdoteCountry}>
              <Text><Emoji name="round_pushpin"/> {this.state.clickedAnecdote.country}</Text>
            </View>
            {this.generateAddAnecdoteToListsButtons()}
          </View>
        </Modal>
        <Modal
          style={[Constants.styles.modal, Constants.styles.modalSuccess]}
          position={"center"}
          ref={"modal_success"}
          swipeToClose={this.state.swipeToClose}
        >
          <View>
            <Animatable.View animation="slideInDown"><Icon name="check" color={Constants.behindTextBackgroundColor} size={50}/></Animatable.View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { anecdoteReducer } = state
  return { anecdoteReducer }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    addAnecdoteToBeTold,
    addAnecdoteAlreadyTold,
    removeAnecdoteAlreadyTold,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(AllAnecdotesListTab);
