/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, FlatList, Text, View, Image, Button, Alert, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Animatable from 'react-native-animatable';
import Modal from 'react-native-modalbox';
import Emoji from 'react-native-emoji';

import {createStackNavigator, createAppContainer} from 'react-navigation';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

import anecdotes from './../../static/anecdotes.json';
import * as Constants from './../../constants.js';
import * as UsefulFunctions from './../UsefulFunctions.js'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { shuffleRandomAnecdote, addAnecdoteToBeTold, addAnecdoteAlreadyTold} from './../../actions/AnecdoteActions';

const instructions = Platform.select({
  ios: 'Cliquez pour générer une nouvelle anecdote, restez appuyés pour l\'ajouter aux listes !',
  android: 'Cliquez pour générer une nouvelle anecdote, restez appuyés pour l\'ajouter aux listes !'
});

export class RandomAnecdoteTab extends React.Component<{}> {
  static navigationOptions = {
    tabBarLabel: 'Aléatoire',
    tabBarIcon: ({ focused, tintcolor }) => (
      <Icon name="autorenew" size={20}  color={Constants.secondaryTextColor} />
    )
  };

  constructor() {
    super();
    this.state = {
      swipeToClose: true,
      clickedAnecdote: {title:'Should be updated soon'}
    };
  }

  openModal(item){
    this.setState({
      clickedAnecdote:item
    });
    this.refs.modal.open();
  }

  closeModal(){
    this.refs.modal.close();
    this.refs.modal_success.open();
    setTimeout(this.refs.modal_success.close,1500);
  }

  generateAddAnecdoteToListsButtons(){

    const textLeftButton="Marquer à raconter";
    const textRightButton="Marquer déjà racontée";

    // CHECK IF ALREADY IN ALREADYTOLD
    const isItAlreadyInToBeTold = this.props.anecdoteReducer.toBeTold.some((anecdote)=>{ // we remove the anecdote from the toBeTold array when marking it as alreadyTold
      return anecdote.title === this.props.anecdoteReducer.homescreenAnecdote.title;
    });
    //the style is standard and redirecting to the right action
    let disabledLeftButton = false;
    let onPressLeftButton = () => { this.props.addAnecdoteToBeTold(this.props.anecdoteReducer.homescreenAnecdote) ; this.closeModal() ;  };
    if(isItAlreadyInToBeTold){
      // if it is already in the list, need to deactivate the button and the action
      onPressLeftButton=() => {};
      disabledLeftButton = true;
    }

    // CHECK IF ALREADY IN TOBETOLD
    const isItAlreadyInAlreadyTold = this.props.anecdoteReducer.alreadyTold.some((anecdote)=>{ // we remove the anecdote from the toBeTold array when marking it as alreadyTold
      return anecdote.title === this.props.anecdoteReducer.homescreenAnecdote.title;
    });
    //the style is standard and redirecting to the right action
    let disabledRightButton = false;
    let onPressRightButton = () => { this.props.addAnecdoteAlreadyTold(this.props.anecdoteReducer.homescreenAnecdote) ; this.closeModal() ; };
    if(isItAlreadyInAlreadyTold){
      // if it is already in the list, need to deactivate the button and the action
      onPressRightButton = () => {};
      disabledRightButton = true;
    }
    // RETURN THE TEMPLATED BUTTON
    return UsefulFunctions.createViewContainingTwoButtons(
      textLeftButton,
      textRightButton,
      onPressLeftButton,
      onPressRightButton,
      disabledLeftButton,
      disabledRightButton
    )
  }

  render() {
    return (
      <View style={Constants.styles.mainContainer}>
        <View style={Constants.styles.randomAnecdoteContainer}>
          <TouchableOpacity
            onPress={ () => this.props.shuffleRandomAnecdote() }
            onLongPress={() => this.refs.modal.open()}
            activeOpacity={0.6}
          >
            <Text style={Constants.styles.randomAnecdote}>« {this.props.anecdoteReducer.homescreenAnecdote.title} »</Text>
          </TouchableOpacity>
        </View>
        <View style={Constants.styles.instructionsContainer}>
          <View>
            <Icon.Button
                style={{padding:10, paddingRight:15}}
                name="autorenew"
                color={Constants.defaultIconColor}
                backgroundColor={Constants.defaultButtonBackgroundColor}
                onPress={ () => this.props.shuffleRandomAnecdote() }
            >
              Générer une nouvelle anecdote
            </Icon.Button>
          </View>
          <View style={{paddingLeft:5}}>
            <View>
              <Icon.Button
                name="add"
                color={Constants.redButtonBackgroundColor}
                iconStyle={{marginRight:0}}
                borderRadius={100}
                borderColor={Constants.redBorderBackgroundColor}
                backgroundColor={Constants.behindTextBackgroundColor}
                borderWidth={1}
                onPress={() => this.refs.modal.open()}>
              </Icon.Button>
            </View>
          </View>
        </View>
        <Modal
          style={[Constants.styles.modal, Constants.styles.modalBottom]}
          position={"bottom"}
          ref={"modal"}
          swipeToClose={this.state.swipeToClose}
        >
          <View style={Constants.styles.modalAnecdoteDetailMainContainer}>
            <View style={Constants.styles.modalAnecdoteDetailAnecdoteTitle}>
              <Text>« {this.props.anecdoteReducer.homescreenAnecdote.title} »</Text>
            </View>
            <View style={Constants.styles.modalAnecdoteDetailAnecdoteCountry}>
              <Text><Emoji name="round_pushpin"/> {this.props.anecdoteReducer.homescreenAnecdote.country}</Text>
            </View>
            {this.generateAddAnecdoteToListsButtons()}
          </View>
        </Modal>
        <Modal
          style={[Constants.styles.modal, Constants.styles.modalSuccess]}
          position={"center"}
          ref={"modal_success"}
          swipeToClose={this.state.swipeToClose}
        >
          <View>
            <Animatable.View animation="slideInDown"><Icon name="check" color={Constants.behindTextBackgroundColor} size={50}/></Animatable.View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { anecdoteReducer } = state
  return { anecdoteReducer }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    shuffleRandomAnecdote,
    addAnecdoteAlreadyTold,
    addAnecdoteToBeTold,
  }, dispatch)
);


export default connect(mapStateToProps, mapDispatchToProps)(RandomAnecdoteTab);
