/**
 * @format
 * @flow
 */

import * as React from 'react';
import { View, Image } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { HeaderButtons, HeaderButton } from 'react-navigation-header-buttons';
import * as Constants from './../../constants.js';

// define IconComponent, color, sizes and OverflowIcon in one place
const MaterialHeaderButton = props => (
  <HeaderButton {...props} IconComponent={MaterialIcons} iconSize={23} color={Constants.redButtonBackgroundColor} />
);

export const MaterialHeaderButtons = props => {
  return (
    <View style={{flexDirection:'row'}}>
      <HeaderButtons
      HeaderButtonComponent={MaterialHeaderButton}
      OverflowIcon={<MaterialIcons size={23} color="white" />}
      {...props}
      />
    </View>

  );
};
export { Item } from 'react-navigation-header-buttons';
