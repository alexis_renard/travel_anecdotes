import {SHUFFLE_ANECDOTE, ADD_ANECDOTE_TO_BE_TOLD, ADD_ANECDOTE_ALREADY_TOLD, REMOVE_ANECDOTE_TO_BE_TOLD, REMOVE_ANECDOTE_ALREADY_TOLD} from './../types.js';

export const shuffleRandomAnecdote = () => (
  {
    type: SHUFFLE_ANECDOTE,
  }
);

export const addAnecdoteToBeTold = anecdoteObject => (
  {
    type: ADD_ANECDOTE_TO_BE_TOLD,
    payload: anecdoteObject,
  }
);

export const addAnecdoteAlreadyTold = anecdoteObject => (
  {
    type: ADD_ANECDOTE_ALREADY_TOLD,
    payload: anecdoteObject,
  }
);

export const removeAnecdoteToBeTold = anecdoteObject => (
  {
    type: REMOVE_ANECDOTE_TO_BE_TOLD,
    payload: anecdoteObject,
  }
);

export const removeAnecdoteAlreadyTold = anecdoteObject => (
  {
    type: REMOVE_ANECDOTE_ALREADY_TOLD,
    payload: anecdoteObject,
  }
);
