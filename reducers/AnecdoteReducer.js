import anecdotes from './../static/anecdotes.json';
import {SHUFFLE_ANECDOTE, INTERACT_HOMESCREEN_ANECDOTE, ADD_ANECDOTE_TO_BE_TOLD, ADD_ANECDOTE_ALREADY_TOLD, REMOVE_ANECDOTE_TO_BE_TOLD, REMOVE_ANECDOTE_ALREADY_TOLD} from './../types.js';

const INITIAL_STATE = {
  // homescreenAnecdote: ,
  homescreenAnecdote: anecdotes.anecdotes[Math.round(Math.random()*(anecdotes.anecdotes.length-1))],
  allAnecdotes: anecdotes.anecdotes,
  toBeTold: [],
  alreadyTold: [],
};

const generateNewRandomAnecdote = (state) => {
  let newAnecdote = {};
  let alreadyMarked = true;
  while(alreadyMarked){ //find one anecdote that is not marked yet (neither in alreadyTold nor toBeTold)
    newAnecdote = state.allAnecdotes[Math.round(Math.random()*(anecdotes.anecdotes.length-1))];
    alreadyMarked = false; //presemption d'innoncence
    for(let anecdote in state.toBeTold){
      if(anecdote.title==newAnecdote.title){
        alreadyMarked = true;
      }
    }
    if(!alreadyMarked){
      for(let anecdote in state.alreadyTold){
        if(anecdote.title==newAnecdote.title){
          alreadyMarked = true;
        }
      }
    }
  }
  return newAnecdote
}

const anecdoteReducer = (state = INITIAL_STATE, action) => {

  const {
    homescreenAnecdote,
    allAnecdotes,
    toBeTold,
    alreadyTold,
  } = state;

  switch (action.type) {
    case SHUFFLE_ANECDOTE:
      // Pulls homescreenAnecdote out of previous state
      // We do not want to alter state directly in case
      let updatedHomescreenAnecdote = generateNewRandomAnecdote(state);

      const shuffleState = {
        ...state,
        homescreenAnecdote : updatedHomescreenAnecdote,
      }
      return shuffleState;

    case ADD_ANECDOTE_ALREADY_TOLD:
      const newAlreadyTold = JSON.parse(JSON.stringify(alreadyTold));
      const newtoBeToldinAlreadyTold = toBeTold.filter(function(anecdote) { // we remove the anecdote from the toBeTold array when marking it as alreadyTold
      	return anecdote.title !== action.payload.title;
      });
      newAlreadyTold.push(action.payload);
      let alreadyToldShuffledHomescreenAnecdote = generateNewRandomAnecdote(state);

      const alreadyToldState = {
        ... state,
        homescreenAnecdote : alreadyToldShuffledHomescreenAnecdote,
        alreadyTold: newAlreadyTold,
        toBeTold: newtoBeToldinAlreadyTold,
      }
      return alreadyToldState;

      case ADD_ANECDOTE_TO_BE_TOLD:
        const newtoBeTold = JSON.parse(JSON.stringify(toBeTold));
        const newAlreadyToldinToBeTold = alreadyTold.filter(function(anecdote) { // we remove the anecdote from the alreadyTold array when marking it as toBeTold
          return anecdote.title !== action.payload.title;
        });
        newtoBeTold.push(action.payload);
        let toBeToldShuffledHomescreenAnecdote = generateNewRandomAnecdote(state);
        const toBeToldState = {
          ... state,
          homescreenAnecdote : toBeToldShuffledHomescreenAnecdote,
          alreadyTold: newAlreadyToldinToBeTold,
          toBeTold: newtoBeTold,
        }
        return toBeToldState;

      case REMOVE_ANECDOTE_TO_BE_TOLD:
        const newRemoveToBeTold = toBeTold.filter(function(anecdote) {
          // we remove the anecdote from the toBeTold array
        	return anecdote.title !== action.payload.title;
        });
        const removeToBeToldState = {
          ... state,
          toBeTold: newRemoveToBeTold,
        }
        return removeToBeToldState;

      case REMOVE_ANECDOTE_ALREADY_TOLD:
        const newRemoveAlreadyTold = alreadyTold.filter(function(anecdote) {
          // we remove the anecdote from the alreadyTold array
        	return anecdote.title !== action.payload.title;
        });
        const removeAlreadyToldState = {
          ... state,
          alreadyTold: newRemoveAlreadyTold,
        }
        return removeAlreadyToldState;

    default:
      return state;
  }
};

export default anecdoteReducer;
