// Imports: Dependencies
import { combineReducers } from 'redux';

import anecdoteReducer from './AnecdoteReducer';

const rootReducer = combineReducers({
  anecdoteReducer,
});

export default rootReducer
