## Format Json

* Id
	* Titre
	* Pays
	* Photo
		* Optionnelle, peut être celle du pays dans lequel se situe l'anecdote
	* Tags
		* Soirée, Dépaysement, Aventure, Nature, Rencontre, Accomplissement

## Liste
* Arrivée en Ecuateur comme un (vrai) touriste
* L'asencion du Huayna Potosi, faire désormais parti du club 6000 avec les skis dans le dos (bonus descente en free-ride)
* The Death Road : 64km de descente en VTT sur la route la plus dangereuse du monde
* Las Pampas de Bolivia : piranhas, cobra, puma, caimans et nage avec les dauphins roses
* Speech du gouvernement Bolivien à la petite communauté de Villa Alcira
* Les matchs de football dans la jungle
* Un pilote amateur sous orage, pluie et de nuit - Scénario catastrophe pour un retour au village par la rivière
* Avion ou bus pour entrer dans la jungle, le choix qui m'a sauvé la vie
* Une intoxication alimentaire amusante
* Sur les traces des dinosaures
* Pantalones blancos et spéléologie aventureuse
* Entre Paris et Montréal, un entretien technique dans la jungle Bolivienne
* Le trekking de la vache égarée / les grandes cascades
* L'homme qui ne savait ni lire ni écrire, mais qui était heureux dans sa jungle
* Traversée de rivière de nuit et entrée dans la jungle
* La folle soirée et l'Uruguayenne
* Le coucher de soleil ardent de Salinas Grandes
* La montagne des 14 couleurs et le romantisme orageux
* Le vent, les étoiles et le phare de Cabo Polonio
* La Isla de la fantasia de San Telmo
* Traversée de favela et marche de los dos hermaos
* Le Carnaval du Brésil, ou la folie humaine
* Première session de surf torse-nu
* Fin de journée sur les plages de Florianopolis
* Les incroyables chutes d'Iguazu
* La folle histoire d'un prof de surf à Pichilemu
* Vivre sans chaussures
* L'histoire d'une chilienne et d'un cours magistral
* Un bustaurant et des Cathédrales de Marbre
* Vivre perdu en Patagonie
* Le rafting extrême de Futaleufu
* L'insolite plage en face de Puerto Montt
* Olmué, ou le repos absolu dans une famille chilienne.
* Pucón et ascension du Villarica avec le gars Max
* Nouvel An dans la baie de Valparaiso
* Le message de Pauline Le Gall
* Découverte de la Patagonie avec les darons
* Majorer un cours de réseau informatique avec du saucisson
* Lydia et le Liberty
* Un mate et un parc en Argentine
* Les bodegas de Mendoza
* Les Casathons de Valparaiso : une idée qui s'est concrétisée
* Un salar pour les gouverner tous : Uyuni
* San Pedro de Atacama : la folie de la playita
* Mon application mobile disponible sur le store !
* Un match de football de seconde division : l'Amérique du Sud ne démérite pas
* Isla de la fantasia de Valparaiso, la culture bat son plein
* La vie à 24 dans une coloc : Casa casa casa Mariposaaaa, ohhhweoh !
* Le surf après les cours
* Laguna verde : la chaleur d'un groupe d'amis sur la plage, mais le froid glacial d'un camping en hiver
* Dans la peuf le Vendredi, sur la planche le Samedi : un rêve qui devient réalité
* Pile ou face, deux reverse ou le paris osé
* 10 jeunes garcons à Pichilemu : Norvège, Danemark, France, Allemagne, Italie et Belgique réunis autour du thème de la guerre
* Les fondas de Valparaiso, un mix impensé et des surprises de taille
* Running canin, ceviche et rencontre Urugayenne
* Entre Pérou et Chili : le fou rire incomprehensible du taxi d'Arica
* "Alemania y Francia bailando juntos"
* Arequipa, bar de contrebande et discussions profondes
* Lima by night: la rue où il ne fallait pas aller
* Le (non) départ en Amérique du Sud : nuit au Marriot pour l'international business man
