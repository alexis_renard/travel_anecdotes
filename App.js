/**
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, FlatList, Text, View, Image, Button, Alert, StatusBar} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation';
// import { YellowBox } from "react-native"
import { MenuProvider } from 'react-native-popup-menu';

import anecdotes from './static/anecdotes.json';
import * as Constants from './constants.js';
import AppContainer from './components/AppContainer';

//redux-persist imports
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux';
import { store, persistor } from './store/store';

//
// YellowBox.ignoreWarnings([
//   "Warning: ViewPagerAndroid has been extracted",
// ])

export default class App extends React.Component<{}> {

  render() {
    return (
      <Provider store={ store }>
        <PersistGate loading={null} persistor={persistor}>
          <MenuProvider>
            <View>
              <StatusBar backgroundColor={Constants.headerBackgroundColor} barStyle='light-content' />
            </View>
            <AppContainer/>
          </MenuProvider>
        </PersistGate>
      </Provider>
    )
  }
}
